/*************************************************************************
                           fich  -  description
                             -------------------
    d�but                : 6 nov. 2011
    copyright            : (C) 2011 par Robert
*************************************************************************/

//---------- Interface de la classe <fich> (fichier fich.h) ------
#if ! defined ( FICH_H_ )
#define FICH_H_
#include <string>

//--------------------------------------------------- Interfaces utilis�es

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// R�le de la classe <fich>
//
//
//------------------------------------------------------------------------ 

class fich
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- M�thodes publiques
    // type M�thode ( liste des param�tres );
    // Mode d'emploi :
    //
    // Contrat :
    //

	bool Parcourir (string&, long&);

//------------------------------------------------- Surcharge d'op�rateurs
    //fich & operator = ( const fich & unfich );
    // Mode d'emploi :
    //
    // Contrat :
    //

	 friend istream & operator >> ( istream & unfic, string& mot );
	 // Mode d'emploi :
	 //
	 // Contrat :
	 //

	 friend ostream & operator << (ostream& os, string& mot);

//-------------------------------------------- Constructeurs - destructeur
    fich ( const fich & unfich );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    fich ( string nomdufichier );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~fich ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- M�thodes prot�g�es

    char LectureCaractere (long&);

//----------------------------------------------------- Attributs prot�g�s


    ifstream file;


};

//--------------------------- Autres d�finitions d�pendantes de <fich>

#endif // FICH_H_

