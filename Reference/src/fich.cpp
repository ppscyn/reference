/*************************************************************************
                           fich  -  description
                             -------------------
    d�but                : 6 nov. 2011
    copyright            : (C) 2011 par Robert
*************************************************************************/

//---------- R�alisation de la classe <fich> (fichier fich.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include syst�me

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//------------------------------------------------------ Include personnel
#include "fich.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- M�thodes publiques
// type fich::M�thode ( liste des param�tres )
// Algorithme :
//
//{
//} //----- Fin de M�thode

bool fich::Parcourir (string& mot, long& numligne)
// Algorithme :
//
{

	bool mottrouve=false;

	int testalpha;

	char carlu;
	char carluprecedent;

	mot = "";

	bool slashavant=false;
	bool symbolespecial=false;


	carlu=LectureCaractere(numligne);


	while (!file.eof())
	{
		symbolespecial=false;
		testalpha=isalnum(carlu);
		if ((testalpha!=0)or(carlu=='_'))
		{
			mottrouve=true;
			mot += carlu;       			//CAS NORMAL

			carlu=LectureCaractere(numligne);

		}
		else								// Cas speciaux
		{


			if (carlu=='#')					// Cas PREPROCESSEUR
			{
				symbolespecial=true;
				do
				{

					carlu=LectureCaractere(numligne);
					testalpha=isalnum(carlu);   //on va juqu'au prochain mot apres le #
				}
				while (testalpha==0);

				do
				{

					carlu=LectureCaractere(numligne);
					testalpha=isalnum(carlu);     //on ne garde pas le prochain mot qui est du genre define, notdefine,...
				}
				while (testalpha!=0);

				// a la fin de ces boucles on est arrives derriere le mot suivant le # et on reprend comme avant

			}


			if (carlu=='\'')
			{
				symbolespecial=true;

				carlu=LectureCaractere(numligne);

				carlu=LectureCaractere(numligne);
				   // on saute trois caracteres et on reprend comme avant
				carlu=LectureCaractere(numligne);
			}


			if (carlu=='\"')
			{
				symbolespecial=true;
				do
				{

					carlu=LectureCaractere(numligne);
				}
				while (carlu!='\"');

				carlu=LectureCaractere(numligne);
			}



			if (carlu=='*')
			{
				symbolespecial=true;
				if (slashavant==true)
				{
					do
					{
						carluprecedent=carlu;

						carlu=LectureCaractere(numligne);
					}
					while ((carluprecedent!='*')or(carlu!='/'));
					slashavant=false;
				}

				carlu=LectureCaractere(numligne);
			}



			if (carlu=='/')
			{
				symbolespecial=true;

				if (slashavant==true)
				{
					do
					{

						carlu=LectureCaractere(numligne);
					}
					while (carlu!='\n');
					slashavant=false;

					carlu=LectureCaractere(numligne);
				}
				else
				{
					slashavant=true;

					carlu=LectureCaractere(numligne);
				}

			}

			if (symbolespecial!=true)
			{

				if (mottrouve==true)      // Autres symboles SPACE,';','=',...
				{
					return true;   // Le mot est deja rempli donc on sort de la fonction
				}
				else
				{						// il n'y a pas de mot avant le symbole actuel
										// on se place sur le caractere suivant et on reprend comme avant

					carlu=LectureCaractere(numligne);


				}
			}

		}

	}

	if (mottrouve==true)
	{
		return true;   // On a un mot juste avant la fin de fichier
	}
	else
	{
		return false;  // On a un symbole juste avant la fin de fichier
	}

}



//------------------------------------------------- Surcharge d'op�rateurs
/*fich & fich::operator = ( const fich & unfich )
// Algorithme :
//
{
} //----- Fin de operator =*/

istream & operator >> ( istream& unfic, string& mot )
// Algorithme :
//
{
	getline(unfic, mot);
	return unfic;
} //----- Fin de M�thode

ostream& operator<<(ostream& os, string& mot)
{
	os << mot;
	return os;
}


//-------------------------------------------- Constructeurs - destructeur
fich::fich ( const fich & unfich )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <fich>" << endl;
#endif
} //----- Fin de fich (constructeur de copie)


fich::fich ( string nomdufichier )
// Algorithme :
//
{

	file.open(nomdufichier.c_str());



#ifdef MAP
    cout << "Appel au constructeur de <fich>" << endl;
#endif
} //----- Fin de fich


fich::~fich ( )
// Algorithme :
//
{

	file.close();

#ifdef MAP
    cout << "Appel au destructeur de <fich>" << endl;
#endif
} //----- Fin de ~fich


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- M�thodes prot�g�es

char fich::LectureCaractere(long& numligne)
// Algorithme :
//
{
	char caractere;
	file.get(caractere);

	if (caractere=='\n')
	{
		numligne++;
	}

	return caractere;
}

