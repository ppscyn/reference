/*************************************************************************
                           stockage  -  description
                             -------------------
    d�but                : 9 nov. 2011
    copyright            : (C) 2011 par sylvain
*************************************************************************/

//---------- R�alisation de la classe <stockage> (fichier stockage.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include syst�me

#include <iostream>
#include <fstream>
#include <string>
using namespace std;
#include <map>
#include <list>
#include <set>
//------------------------------------------------------ Include personnel
#include "stockage.h"
#include "fich.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- M�thodes publiques
// type stockage::M�thode ( liste des param�tres )
// Algorithme :
//
//{
//} //----- Fin de M�thode

void stockage::traitement (string plouf, int nbfichiers, bool optionE, bool optionK)
// Algorithme :
//
{
bool aStocker = false;

// FL
/*
liste* tableau = new liste [nbfichiers];
tableau[1].push_back(8);

tableau[1].push_back(18);

// permet de parcourir toute la list,


liste::iterator it;
	for(it = tableau[1].begin(); it!=tableau[1].end(); it++) // v�rifier que begin n'est pas �gal � end sinon �a merde
	{
	cout << *it << endl;
	}

*/



  //map<char,std::list*<int>> mymap;

// marche
  map<string,liste*> mymap;

  map<string,liste*>::iterator it;


  //liste** tableau = new liste* [nbfichiers]; // version tableau qui pointe vers des lists ?
  liste* tableau = new liste [nbfichiers];
  mymap["patate"] = tableau;

  tableau[1].push_back(8);
  // tableau[1]->push_back(8);


  liste::iterator ittableau;

  it = mymap.find("patate");
  ittableau = it->second[1].begin();

	cout << *ittableau << endl;

 // ittableau = mymap.find("patate")->second->tableau[1].begin();

//	cout << mymap.find("patate")->second[1].begin() << endl;








/*
map<char,int> mymap;
map<char,int>::iterator it;

  mymap['a']=10;
  mymap['b']=30;
  mymap['c']=50;
  mymap['d']=70;

  for ( it=mymap.begin() ; it != mymap.end(); it++ )
     cout << (*it).first << " => " << (*it).second << endl;
*/

//MotClefAccesRapide.insert(5);
//MotClefAccesRapide.insert(15);
//set<int>::iterator it;



cout << "plouffe" << endl;

return aStocker;
} //----- Fin de M�thode


void stockage::initialisation (string plouf) // passer le nom du fichier keyword en parametre
// but prendre tous les mots clef et les mettre dans un map � acc�s rapide
{
string motLu; // mot r�cup�r� par la fonction lecture
long numligne; // num�ro de la ligne pour la fonction lecture
bool motTrouve; // est-ce que la fonction parcourir a trouve un mot



// lecture du fichier pass� en param�tre, si pas de fichier en param�tre (genre plouf vide, prendre motclef c++)


//	MotClefAccesRapide[i]=Parcourir (string&);

set<string>::iterator itset;

numligne=1;

fich fichier(plouf);

do
{
	motTrouve=fichier.Parcourir(motLu,numligne);
	if (motTrouve==true)
	{
		MotClefAccesRapide.insert(motLu);
	}

}
while(motTrouve==true);

// pour le lire
/*
for (itset=MotClefAccesRapide.begin(); itset!=MotClefAccesRapide.end(); itset++)
   cout << *itset << endl;
*/



//fin boucle



}


bool stockage::motclef(string& motATester)
// Algorithme :
//
{
bool motclef=true;
set<string>::iterator itset;

itset = MotClefAccesRapide.find(motATester); // On v�rifie si le mot clef est dans le set MotClefAccesRapide

if (itset==MotClefAccesRapide.end())
	{
		// on a trouv� un identificateur (sinon mettre itset!=MotClefAccesRapide.end() pour les mots clefs)
		motclef = false;
		cout << *itset <<endl;
	}

return motclef;
}



//------------------------------------------------- Surcharge d'op�rateurs
/*stockage & stockage::operator = ( const stockage & unstockage )
// Algorithme :
//
{
} //----- Fin de operator =
*/

//-------------------------------------------- Constructeurs - destructeur
stockage::stockage ( const stockage & unstockage )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <stockage>" << endl;
#endif
} //----- Fin de stockage (constructeur de copie)


stockage::stockage ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <stockage>" << endl;
#endif
} //----- Fin de stockage


stockage::~stockage ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <stockage>" << endl;
#endif
} //----- Fin de ~stockage


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- M�thodes prot�g�es

