/*************************************************************************
                           stockage  -  description
                             -------------------
    d�but                : 9 nov. 2011
    copyright            : (C) 2011 par sylvain
*************************************************************************/

//---------- Interface de la classe <stockage> (fichier stockage.h) ------
#if ! defined ( STOCKAGE_H_ )
#define STOCKAGE_H_
using namespace std;
#include <string>
#include <list>
#include <map>
#include <set>
#include <iostream>


//--------------------------------------------------- Interfaces utilis�es

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

/*typedef struct liste liste;
struct liste
{
	list<int> liste;
};
*/

typedef list<int> liste;


//------------------------------------------------------------------------ 
// R�le de la classe <stockage>
//
//
//------------------------------------------------------------------------ 

class stockage
{
//----------------------------------------------------------------- PUBLIC




public:
//----------------------------------------------------- M�thodes publiques
    // type M�thode ( liste des param�tres );
    // Mode d'emploi :
    //
    // Contrat :
    //

	bool traitement (string plouf, int nbfichiers, bool optionE, bool optionK);
    // Mode d'emploi :
    //
    // Contrat :
    //

	void initialisation (string plouf);
    // Mode d'emploi :
    //
    // Contrat :
    //

	bool motclef (string& plouf); // on dit si c'est un mot clef ou non
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------- Surcharge d'op�rateurs
    stockage & operator = ( const stockage & unstockage );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    stockage ( const stockage & unstockage );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    stockage ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~stockage ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- M�thodes prot�g�es

//----------------------------------------------------- Attributs prot�g�s

   //map<int,string> MotClefAccesRapide;
   // set<int> MotClefAccesRapide;
    set<string> MotClefAccesRapide;

};

//--------------------------- Autres d�finitions d�pendantes de <stockage>

#endif // STOCKAGE_H_

